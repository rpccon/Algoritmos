/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algoritmos;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Johanny
 */
public class Operations {
    
    Scanner user_input ;
    public Operations(){
        user_input = new Scanner( System.in ); // tomar datos del cliente
    }
   
    public void Menu(){  // main view  
        String opcion = "";
        System.out.println("*********** Menu ***********\n\nIndique el número que corresponde a su opción\n 1. Alg 1\n 2. Alg 2 y 3");
        opcion = user_input.next();
        
        if(opcion.equals("1")){ 
            int[] arreglo = new int[4]; // arreglo inicial
            for(int i=0;i<4;i++){ // para ingresar los 4 números del arreglo de la opción 1
                System.out.println("Indique el número "+String.valueOf(i+1));
                String num = user_input.next(); // obtener dato del usuario
                arreglo[i] = Integer.valueOf(num);
            }             
            algUno(arreglo[0],arreglo[1],arreglo[2],arreglo[3]);// envia los números como parámetro al método
        }
        else if(opcion.equals("2")){
            
            System.out.println("Indique el tamaño del arreglo(número)....");
            String num = user_input.next();// solicita el tamaño al usuario
            algDos(Integer.valueOf(num));// envia tamaño para usar en arreglo en el segundo algoritmo           
        }
        else{ // lo que sea que se indique que no esté en el menú, es un error
           System.out.println("No indicó ninguna de las opciones existentes");
           Menu();
        }
    }
    
    public void algUno(int a,int b, int c, int d){
        int[] arreglo = new int[4]; // definición de arreglo
        arreglo[0]=a;
        arreglo[1]=b;
        arreglo[2]=c; // asignación de números que se envían por parámetro en las posiciones de l arreglo
        arreglo[3]=d; 
        System.out.println("************* ("+ String.valueOf(arreglo[0])+" x "+String.valueOf(arreglo[arreglo.length-1])+") - ("+String.valueOf(arreglo[1])+" - "+String.valueOf(arreglo[2])+")");// mostrar la estructura física de la operación     
        int varA = arreglo[0] * arreglo[arreglo.length-1]; // multiplicación de extremos
        int varB = arreglo[1] - arreglo[2]; // resta de medios        
        System.out.println("************* El resultado es: "+String.valueOf(varA-varB));           
        backToMenu(); // Regresar al menú principal
    }
    
    public void algDos(int tam){ // recibe tamaño del arreglo 
           int[] arreglo = new int[tam];
           Random rand = new Random();
           for(int i=0; i<arreglo.length;i++){ // genera números random al nuevo arreglo a comparar
               arreglo[i] = rand.nextInt(50) + 1;
           }                     
           System.out.println("El arreglo A es: \n"); 
           String arr = "[";
           for(int i=0;i<arreglo.length;i++){ //recorren el array para sacar los números
               if(i == arreglo.length-1){
                   arr+= String.valueOf(arreglo[i]); // cuando es el último registro...
               }
               else{
                   arr+=String.valueOf(arreglo[i])+",";
               }
           }
           arr+="]"; // construcción del array
           System.out.println(arr); // muestra el arreglo al usuario          
           algTres(arreglo); // punto 3 envía arreglo como parámetro para generar otro arreglo y comparar
           backToMenu(); // regresar a menú        
    }
    
    public void algTres(int[] array){
        int[] newArr = new int[array.length]; // genera nuevo array        
        Random rand = new Random();                
        for(int i=0; i<newArr.length;i++){      
            newArr[i] = rand.nextInt(50) + 1; // asigna random numbers al array
        }        
        System.out.println("El arreglo B es: \n");
        String arr = "[";
        for(int i=0;i<newArr.length;i++){ // construye el segundo array
            if(i == newArr.length-1){
                arr+= String.valueOf(newArr[i]);
            }
            else{
                arr+=String.valueOf(newArr[i])+",";
            }
        }
        arr+="]";
        System.out.println(arr);   // lo muestra al usuario     
        
        int contador = 0;
        for(int i=0;i<array.length;i++){// recorres los 2 arrays en busca de similitudes
            if(array[i]==newArr[i]){
                System.out.println("Hay datos similares en la pos "+String.valueOf(i)+" con el número: "+String.valueOf(newArr[i]));
                contador++; // para determinar si no hay incidencias entre los arrays
            }
        }
        
        if(contador==0){ // no hay similitudes
            System.out.println("No hay datos similares en los arreglos");
        }
    }
    
    public void backToMenu(){ // volver al menú principal
        System.out.println("Presione cualquier cosa para regresar a menu........");
        String back = user_input.next();
        Menu();      
    }
    
}
